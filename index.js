const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

//connection to mongoDB Atlas
mongoose.connect("mongodb+srv://devjeff:jeffjeff@wdc028-course-booking.o1lar.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	});
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


const usersSchema = new mongoose.Schema({
	username: String,
	password: String
})

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})


//Create a model
const Task = mongoose.model("Task", taskSchema);
const Users = mongoose.model("Users", usersSchema);


app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Create a POST Route to create a new task
app.post("/tasks",(req,res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		 if(result != null && result.name == req.body.name){
		 	return res.send("Duplicate task found");
		 }
		 else{
		 	let newTask = new Task({
		 		name: req.body.name
		 	});

		 	newTask.save((saveErr,savedTask) => {
		 		if(saveErr){
		 			return console.error(SaveErr);
		 		}
		 		else{
		 			return res.status(201).send("New task created");
		 		}
		 	})
		 }
	})
})


//Create a GET Request

app.get("/tasks", (req,res) => {
	Task.find({},(err,result) => {
	if(err){
		return console.log(err);
	}
	else{
		return res.status(200).json({
			data: result
		})
	 }

	})
})

//ACTIVITY SECTION

//Create a POST Route to create a new task
app.post("/signup",(req,res) => {
	Users.findOne({username: req.body.username}, (err, result) => {
		 if(result != null && result.username == req.body.username){
		 	return res.send("Duplicate task found");
		 }
		 else{
		 	let newUser = new Users({
		 		username: req.body.username,
		 		password: req.body.password
		 	});

		 	newUser.save((saveErr,savedUser) => {
		 		if(saveErr){
		 			return console.error(SaveErr);
		 		}
		 		else{
		 			return res.status(201).send("New user registered");
		 		}
		 	})
		 }
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}`));

